﻿using Core;
using Notifications.Core.Content;
using Notifications.Core.Enums;
using Notifications.Core.Exceptions;
using Notifications.Core.NotificationProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Util.DBUtil;

namespace Notifications.Core
{
    public class NotificationsProcessor
    {
        //change return type to list of msgs
        public Boolean ProcessPendingNotifications(IDictionary<string, string> parameters)
        {
            Boolean success = false;
            try
            {
                string providerId = parameters["ProviderId"];
                if (!string.IsNullOrWhiteSpace(providerId))
                {
                    NotificationsFactory factory = new NotificationsFactory();
                    NotificationProvider np = factory.GetProvider(providerId) as NotificationProvider;
                    if (np != null)
                    {
                        IList<Notification> notifications = GetPendingNotifications(parameters, np.Channel);
                        success = np.Send(notifications);
                    }
                    else
                    {
                        throw new ProviderError("Provider not found ProviderID: " + providerId + ", ProcessorId" + parameters["ProcessorId"]); // not available in factory
                    }
                }
                else
                {
                    throw new ProviderError("Provider not specfied" + parameters["ProcessorId"]); // not available from the job
                }
            }
            catch (Exception ex)
            {
                NotificationsLogger.Logger.Error("Error in ProcessPendingNotifications", ex);
            }
            return success;
        }

        private IList<Notification> GetPendingNotifications(IDictionary<string, string> parameters, NotificationChannel channel)
        {
            IList<Notification> notifications = new List<Notification>();
            DBFactory dbfactory = DBFactory.getInstance();
            IDBOps dbOps = dbfactory.getDBOpsClass();
            SpaReaderEx spaReader = null;
            string URI = parameters.ContainsKey("URL") ? parameters["URL"] : "";
            int NotificationType;
            string strQueueId = string.Empty, strCenterId = string.Empty, strOrganizationId = string.Empty, strCountryCode = string.Empty;
            NotificationCategory category = NotificationCategory.Any;
            NotificationPriority priority = NotificationPriority.Any;
            if (parameters.ContainsKey("QueueCategory"))
            {
                category = (NotificationCategory)Enum.Parse(typeof(NotificationCategory), parameters["QueueCategory"], true);
            }
            if (parameters.ContainsKey("QueuePriority"))
            {
                priority = (NotificationPriority)Enum.Parse(typeof(NotificationPriority), parameters["QueuePriority"], true);
            }
            try
            {
                if (dbOps.OpenConnection() == 0)
                {
                    if (!parameters.ContainsKey("ProcName"))
                    {
                        dbOps.StoredProcedure("Sp_notifications_getpendingnotifications");
                        dbOps.AddCharParameter("@providerId", parameters["ProviderId"]);
                        dbOps.AddCharParameter("@processorId", parameters["ProcessorId"]);
                        dbOps.AddIntParameter("@QueuePriority", (int)priority);
                        dbOps.AddIntParameter("@QueueCategory", (int)category);
                        if (parameters.ContainsKey("NoOfRecords"))
                            dbOps.AddIntParameter("@NoOfRecords", Convert.ToInt16(parameters["NoOfRecords"]));
                    }
                    else
                    {
                        //TBD
                    }
                    spaReader = dbOps.Execute();
                    switch (channel)
                    {
                        case NotificationChannel.EMAIL:
                            {
                                while (spaReader.Read())
                                {
                                    NotificationType = int.Parse(spaReader.Get("NotificationType").ToString());
                                    strQueueId = spaReader.Get("EmailQueueId").ToString();
                                    strCenterId = spaReader.Get("CenterId").ToString();
                                    strOrganizationId = spaReader.Get("OrganizationId").ToString();
                                    string Email = spaReader.Get("EmailID").ToString();
                                    string EmailCc = spaReader.Get("CC").ToString();
                                    IList<string> CC = string.IsNullOrWhiteSpace(EmailCc) ? null : EmailCc.Split(';');
                                    EMailNotification msg = new EMailNotification(strCenterId, NotificationType, strQueueId, Email, CC);

                                    MacroProcessor macroProcessor = new MacroProcessor();

                                    msg.Subject = macroProcessor.ProcessMacros(spaReader.Get("Subject").ToString(), spaReader.Get("NotificationMacrosContent").ToString(), NotificationType);
                                    if (NotificationType != 12 && NotificationType != 36 && NotificationType != 75)
                                        msg.HTMLContent = macroProcessor.ProcessMacros(spaReader.Get("HTMLTemplate").ToString(), spaReader.Get("NotificationMacrosContent").ToString(), NotificationType, spaReader.Get("OrgURL").ToString(), spaReader.Get("AttachmentParams").ToString());
                                    else
                                    {
                                        msg.HTMLContent = System.Web.HttpUtility.HtmlDecode(spaReader.Get("HTMLTemplate").ToString());
                                        msg.HTMLContent = macroProcessor.ProcessMacros(msg.HTMLContent, spaReader.Get("NotificationMacrosContent").ToString(), NotificationType, spaReader.Get("OrgURL").ToString(), spaReader.Get("AttachmentParams").ToString());
                                    }

                                    if (string.IsNullOrWhiteSpace(msg.HTMLContent))
                                    {
                                        msg.PlainContent = macroProcessor.ProcessMacros(spaReader.Get("PlainTemplate").ToString(), spaReader.Get("NotificationMacrosContent").ToString(), NotificationType, spaReader.Get("OrgURL").ToString(), spaReader.Get("AttachmentParams").ToString());
                                        msg.HTMLContent = msg.PlainContent;
                                    }
                                    else
                                    {
                                        msg.PlainContent = macroProcessor.ProcessMacros(spaReader.Get("PlainTemplate").ToString(), spaReader.Get("NotificationMacrosContent").ToString(), NotificationType, spaReader.Get("OrgURL").ToString(), spaReader.Get("AttachmentParams").ToString());
                                    }

                                    msg.NotificationFk = spaReader.Get("NotificationFK") != DBNull.Value ? Convert.ToInt64(spaReader.Get("NotificationFK")) : 0;
                                    msg.QueuePK = spaReader.Get("EmailQueuePK") != DBNull.Value ? Convert.ToInt64(spaReader.Get("EmailQueuePK")) : 0;
                                    msg.TryPK = spaReader.Get("EmailTriesPK") != DBNull.Value ? Convert.ToInt64(spaReader.Get("EmailTriesPK")) : 0;
                                    msg.Channel = NotificationChannel.EMAIL;
                                    msg.Class = (NotificationClass)Convert.ToInt16(spaReader.Get("Class"));
                                    msg.Category = (NotificationCategory)Convert.ToInt16(spaReader.Get("Category"));
                                    msg.CenterId = spaReader.Get("CenterId").ToString();
                                    msg.OrganizationId = strOrganizationId;
                                    msg.TrakingData = URI;
                                    if (msg.Category == NotificationCategory.Marketing)
                                    {
                                        msg.HTMLContent += FooterUnsubscribeLink(spaReader.Get("OrgURL").ToString(), strQueueId, Email);
                                    }

                                    if (NotificationType != 20 && NotificationType != 11 && NotificationType != 10 && NotificationType != 101 && NotificationType != 102)
                                    {
                                        msg.FromName = spaReader.Get("EmailSenderDisplayName").ToString();
                                        msg.FromEmail = spaReader.Get("EmailIdForSendingMails").ToString();
                                    }
                                    if (!string.IsNullOrWhiteSpace(spaReader.Get("AttachmentParams").ToString()) && NotificationType != 75)
                                    {
                                        Attachment att = Attachment.GenerateAttachment(NotificationType, spaReader.Get("AttachmentParams").ToString(), spaReader.Get("OrgURL").ToString(), strQueueId);
                                        if (att != null)
                                        {
                                            msg.Attachments.Add(att);
                                        }
                                    }
                                    if (NotificationType == 75 || NotificationType == 22)
                                    {
                                        Attachment att = Attachment.GenerateAttachment(NotificationType, msg.HTMLContent, spaReader.Get("OrgURL").ToString(), strQueueId);
                                        if (att != null)
                                        {
                                            msg.Attachments.Add(att);
                                        }
                                    }
                                    notifications.Add(msg);
                                }
                                break;

                            }
                        case NotificationChannel.SMS:
                            {
                                while (spaReader.Read())
                                {
                                    NotificationType = int.Parse(spaReader.Get("NotificationType").ToString());
                                    strQueueId = spaReader.Get("TextMessageQueueId").ToString();
                                    strCenterId = spaReader.Get("CenterId").ToString();
                                    strOrganizationId = spaReader.Get("OrganizationId").ToString();
                                    TextMessageNotification msg = new TextMessageNotification(strCenterId, NotificationType, strQueueId, spaReader.Get("MobileNumber").ToString());

                                    msg.NotificationFk = spaReader.Get("NotificationFK") != DBNull.Value ? Convert.ToInt64(spaReader.Get("NotificationFK")) : 0;
                                    msg.QueuePK = spaReader.Get("TextMessageQueuePK") != DBNull.Value ? Convert.ToInt64(spaReader.Get("TextMessageQueuePK")) : 0;
                                    msg.TryPK = spaReader.Get("TextMessageTriesPK") != DBNull.Value ? Convert.ToInt64(spaReader.Get("TextMessageTriesPK")) : 0;
                                    msg.Channel = NotificationChannel.SMS;
                                    msg.Class = (NotificationClass)Convert.ToInt16(spaReader.Get("Class"));
                                    msg.Category = (NotificationCategory)Convert.ToInt16(spaReader.Get("Category"));

                                    MacroProcessor macroProcessor = new MacroProcessor();
                                    msg.Content = System.Web.HttpUtility.HtmlDecode(spaReader.Get("PlainTemplate").ToString());
                                    msg.Content = macroProcessor.ProcessMacros(msg.Content, spaReader.Get("NotificationMacrosContent").ToString(), NotificationType);
                                    msg.OrganizationId = strOrganizationId;
                                    msg.CountryPhoneCode = String.Concat("+", spaReader.Get("PhoneCode").ToString());
                                    if (NotificationType != 12)
                                        msg.TrakingData = spaReader.Get("HashCode").ToString();
                                    notifications.Add(msg);
                                }
                                break;
                            }
                        case NotificationChannel.VOICE:
                            {
                                while (spaReader.Read())
                                {
                                }
                                break;
                            }
                        case NotificationChannel.PUSH:
                            {
                                while (spaReader.Read())
                                {
                                    NotificationType = int.Parse(spaReader.Get("NotificationType").ToString());
                                    strQueueId = spaReader.Get("PushMessageQueueId").ToString();
                                    strCenterId = spaReader.Get("CenterId").ToString();
                                    strOrganizationId = spaReader.Get("OrganizationId").ToString();
                                    PushNotification msg = new PushNotification(strCenterId, NotificationType, strQueueId, spaReader.Get("TokenID").ToString());
                                    // SentTo, RegistrationID, Category,CenterID, Class & Channel updated
                                    msg.NotificationFk = spaReader.Get("NotificationFK") != DBNull.Value ? Convert.ToInt64(spaReader.Get("NotificationFK")) : 0;
                                    msg.QueuePK = spaReader.Get("PushMessageQueuePK") != DBNull.Value ? Convert.ToInt64(spaReader.Get("PushMessageQueuePK")) : 0;
                                    msg.TryPK = spaReader.Get("PushMessageTriesPK") != DBNull.Value ? Convert.ToInt64(spaReader.Get("PushMessageTriesPK")) : 0;
                                    msg.Channel = NotificationChannel.PUSH;
                                    msg.Class = (NotificationClass)Convert.ToInt16(spaReader.Get("Class"));
                                    msg.Category = (NotificationCategory)Convert.ToInt16(spaReader.Get("Category"));
                                    msg.AppFK = Convert.ToInt16(spaReader.Get("AppFK"));    // appfk
                                    MacroProcessor macroProcessor = new MacroProcessor();
                                    msg.Message = System.Web.HttpUtility.HtmlDecode(spaReader.Get("PlainTemplate").ToString());
                                    msg.Message = macroProcessor.ProcessMacros(msg.Message, spaReader.Get("NotificationMacrosContent").ToString(), NotificationType);

                                    msg.Payload = macroProcessor.processPayload(spaReader.Get("PayloadContent").ToString());  //push message payload
                                    msg.OrganizationId = strOrganizationId;
                                    notifications.Add(msg);
                                }
                                break;
                            }
                    }
                }
                else
                {
                    throw new Exception("Could not open connection to database");
                }
            }
            catch (Exception ex)
            {
                NotificationsLogger.Logger.Error("Error in executing GetPendingNotifications", ex);
                IDBOps dbOpsex = dbfactory.getDBOpsClass();
                SpaReaderEx spaReaderex = null;
                try
                {
                    if (dbOpsex.OpenConnection() == 0)
                    {
                        dbOpsex.StoredProcedure("Sp_notifications_cleanpendingrecords");
                        dbOpsex.AddCharParameter("@providerId", parameters["ProviderId"]);
                        dbOpsex.AddCharParameter("@processorId", parameters["ProcessorId"]);
                        spaReaderex = dbOpsex.Execute();
                    }
                }
                catch (Exception e)
                {
                    NotificationsLogger.Logger.Error("Error in executing cleanpendingrecords", e);
                }
                finally
                {
                    if (spaReaderex != null)
                        spaReaderex.Close(true);

                    dbOpsex.CloseConnection();
                }
            }
            finally
            {
                if (spaReader != null)
                    spaReader.Close(true);

                dbOps.CloseConnection();
            }
            return notifications;
        }

        private string FooterUnsubscribeLink(string OrgURL, string MessageId, string Email)
        {
            string footer = string.Empty;
            string token = Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Concat(MessageId, "#", Email)));
            footer = "<br/><br/><br/>If you do not want to receive emails, <a href='" + OrgURL + "/api/v100/services/Guest/unsubscribe.aspx?token=" + token + "'>click here</a>";
            return footer;
        }
    }
}
