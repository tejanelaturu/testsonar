﻿using Notifications.Core.Content;
using Notifications.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Util.DBUtil;

namespace Notifications.Core
{
    public interface INotificationProvider
    {
        NotificationChannel Channel { get; }
        int? ValidatorSequence { get; }
        ValidationLevel CriteriaForValidation { get; }

        Boolean Send(IEnumerable<Notification> notifications);
        void UpdateMessageStatusBulk();
        void UpdateMessageStatus(string response);
        void PollMessageStatus();
        bool PostMessageReply(NotificationChannel medium, int messageType, IDictionary<string, string> details, object content, int receiptLogid);

        SpaReaderEx GetDetailsForDesktopNotification(int receiptLogid);
        //void UpdateCallDetails();
        //CallDetails getLastCallDetails(string strGuestID, string strOpportunityID = null);
    }
}
