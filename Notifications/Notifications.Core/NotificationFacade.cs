﻿
using Notifications.Core.Configuration;
using Notifications.Core.Content;
using Notifications.Core.Enums;
using Notifications.Core.NotificationProviders;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Util.DBUtil;

namespace Notifications.Core
{
    public class NotificationFacade
    {
        public NotificationFacade()
        {
        }

        public int LogPostedReply(int receiptLogId, string holderId, string dataPosted, bool postSuccessful)
        {
            IDBOps dbOps = null;
            SpaReaderEx spaReader = null;
            int newRecieptLogId = 0;

            try
            {
                dbOps = DBFactory.getInstance().getDBOpsClass();
                if (dbOps.OpenConnection() == 0)
                {
                    dbOps.StoredProcedure("Sp_messenger_logpostedreply");
                    dbOps.AddIntParameter("@ReceiptLogId", receiptLogId);
                    dbOps.AddCharParameter("@HolderId", holderId);
                    dbOps.AddCharParameter("@DataPosted", dataPosted);
                    dbOps.AddBoolParameter("@PostSuccessful", postSuccessful);
                    spaReader = dbOps.Execute();
                    if (spaReader != null && spaReader.HasRows)
                    {
                        if (spaReader.Read())
                        {
                            int.TryParse(spaReader.Get("ReceiptLogId").ToString(), out newRecieptLogId);
                        }
                    }
                }
                else
                {
                    throw new Exception("Could not open connection to database");
                }
            }
            catch (Exception ex)
            {
                NotificationsLogger.Logger.Error("Error while logging message received", ex);
            }
            finally
            {
                if (dbOps != null)
                    dbOps.CloseConnection();
            }
            return newRecieptLogId;
        }

        public Boolean CanMessage(NotificationChannel channel, string holderId, string parentId, string countryCode)
        {
            Boolean canDoIt = false;
            Manager manager = Manager.GetManager();
            canDoIt = !string.IsNullOrWhiteSpace(manager.GetActiveProviderId(channel, NotificationClass.ALL, countryCode, holderId, parentId));

            return canDoIt;
        }

        public string GetMessageDetails(string responseId, string messageId, NotificationChannel channel, out string QueueID)
        {
            IDBOps dbOps = null;
            SpaReaderEx spaReader = null;
            QueueID = string.Empty;
            string CenterId = null;
            try
            {
                DBFactory dbfactory = DBFactory.getInstance();
                dbOps = dbfactory.getDBOpsClass();
                dbOps.OpenConnection();
                dbOps.StoredProcedure("Sp_messaging_getsentmessagedetails");
                dbOps.AddCharParameter("@ResponseId", responseId);
                dbOps.AddCharParameter("@MessageId", messageId);
                dbOps.AddBoolParameter("@MessageMedium", channel == NotificationChannel.EMAIL);
                spaReader = dbOps.Execute();
                while (spaReader.Read())
                {
                    CenterId = spaReader.Get("CenterId").ToString();
                    //QueueID = spaReader.Get("QueueID").ToString();
                }
            }
            catch (Exception ex)
            {
                NotificationsLogger.Logger.Error("Error while geting message Details  MessageID : " + messageId + " responseid : " + responseId, ex);
            }
            finally
            {
                dbOps.CloseConnection();
            }
            return CenterId;
        }

        /// <summary>
        /// This message will always dump message reponse to commonDB set
        /// </summary>
        public Boolean DumpMessageResponse(string ProviderID, string strResponseText, NotificationChannel channel)
        {
            Boolean success = false;
            IDBOps dbOps = null;

            try
            {
                dbOps = DBFactory.getInstance().getDBOpsClass(DBUseType.UseOptional, "commonDBConnection"); //Get connection of commonDB in order to dump message response
                if (dbOps.OpenConnection() == 0)
                {
                    dbOps.StoredProcedure("Sp_messenger_DumpMessageResponse");
                    dbOps.AddCharParameter("@Messengerid", ProviderID);
                    dbOps.AddCharParameter("@ResponseText", strResponseText);
                    dbOps.AddIntParameter("@NotificationChannel", (int)channel);
                    dbOps.Execute();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                NotificationsLogger.Logger.Error("Error while clearing settings", ex);
            }
            finally
            {
                if (dbOps != null)
                    dbOps.CloseConnection();
            }
            return success;
        }


        public Boolean DumpEmailResponse(string ProviderID, List<Response> responses)
        {
            Boolean success = false;
            IDBOps dbOps = null;

            try
            {
                dbOps = DBFactory.getInstance().getDBOpsClass(DBUseType.UseOptional, "commonDBConnection");
                if (dbOps.OpenConnection() == 0)
                {
                    DataTable dt = GetStructuredResponses(responses, ProviderID);
                    dbOps.StoredProcedure("Sp_notifications_DumpEmailResponse");
                    dbOps.AddStructuredParameter("@ListNotificationStatusUpdateList", dt);
                    dbOps.Execute();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                NotificationsLogger.Logger.Error("Error while clearing settings", ex);
            }
            finally
            {
                if (dbOps != null)
                    dbOps.CloseConnection();
            }
            return success;
        }

        private DataTable GetStructuredResponses(List<Response> responses, string ProviderID)
        {
            DataTable dtResponses = null;

            if (responses != null)
            {
                dtResponses = new DataTable();
                DataColumn colString = new DataColumn("ResponseText");
                colString.DataType = System.Type.GetType("System.String");
                dtResponses.Columns.Add(colString);

                colString = new DataColumn("TokenId");
                colString.DataType = System.Type.GetType("System.String");
                dtResponses.Columns.Add(colString);

                colString = new DataColumn("QueueFK");
                colString.DataType = System.Type.GetType("System.Int64");
                dtResponses.Columns.Add(colString);

                colString = new DataColumn("TriesFK");
                colString.DataType = System.Type.GetType("System.Int64");
                dtResponses.Columns.Add(colString);

                colString = new DataColumn("CenterId");
                colString.DataType = System.Type.GetType("System.String");
                dtResponses.Columns.Add(colString);

                colString = new DataColumn("NotificationFK");
                colString.DataType = System.Type.GetType("System.Int64");
                dtResponses.Columns.Add(colString);

                colString = new DataColumn("NotificationType");
                colString.DataType = System.Type.GetType("System.Int32");
                dtResponses.Columns.Add(colString);

                colString = new DataColumn("SentStatus");
                colString.DataType = System.Type.GetType("System.Int32");
                dtResponses.Columns.Add(colString);

                colString = new DataColumn("ProviderId");
                colString.DataType = System.Type.GetType("System.String");
                dtResponses.Columns.Add(colString);

                colString = new DataColumn("AdditionalData");
                colString.DataType = System.Type.GetType("System.String");
                dtResponses.Columns.Add(colString);

                colString = new DataColumn("SentTo");
                colString.DataType = System.Type.GetType("System.String");
                dtResponses.Columns.Add(colString);

                colString = new DataColumn("Opens");
                colString.DataType = System.Type.GetType("System.Int32");
                dtResponses.Columns.Add(colString);

                colString = new DataColumn("Clicks");
                colString.DataType = System.Type.GetType("System.Int32");
                dtResponses.Columns.Add(colString);

                colString = new DataColumn("Bounce");
                colString.DataType = System.Type.GetType("System.Int32");
                dtResponses.Columns.Add(colString);

                colString = new DataColumn("MarkAsSpam");
                colString.DataType = System.Type.GetType("System.Int32");
                dtResponses.Columns.Add(colString);

                colString = new DataColumn("CreatedDate");
                colString.DataType = System.Type.GetType("System.DateTime");
                dtResponses.Columns.Add(colString);

                colString = new DataColumn("UpdatedDate");
                colString.DataType = System.Type.GetType("System.DateTime");
                dtResponses.Columns.Add(colString);

                colString = new DataColumn("ProcessedStatus");
                colString.DataType = System.Type.GetType("System.Boolean");
                dtResponses.Columns.Add(colString);

                DataRow drResponse;
                foreach (Response response in responses)
                {
                    drResponse = dtResponses.NewRow();
                    drResponse["ResponseText"] = response.ResponseText;
                    drResponse["TokenId"] = response.ResponseId;
                    drResponse["QueueFK"] = response.QueuePK;
                    drResponse["TriesFK"] = response.TryPK;
                    drResponse["CenterId"] = string.IsNullOrWhiteSpace(response.CenterId) ? null : response.CenterId;
                    drResponse["NotificationFK"] = response.NotificationFk;
                    drResponse["NotificationType"] = response.NotificationType;
                    drResponse["SentStatus"] = response.Status;
                    drResponse["ProviderId"] = ProviderID;
                    drResponse["AdditionalData"] = response.AdditionalData;
                    drResponse["SentTo"] = response.SentTo;
                    drResponse["Opens"] = response.Opens;
                    drResponse["Clicks"] = response.Clicks;
                    drResponse["Bounce"] = response.Bounce;
                    drResponse["MarkAsSpam"] = response.Status;
                    drResponse["ProcessedStatus"] = false;

                    dtResponses.Rows.Add(drResponse);
                }
            }

            return dtResponses;
        }
        public IList<INotificationProvider> GetAllActiveProviders(NotificationChannel channel, string parentId, string holderId)
        {
            IList<INotificationProvider> messengers = null;
            messengers = Manager.GetManager().GetAllActiveProviders(channel, parentId, holderId);
            return messengers;
        }

        public IList<INotificationProvider> GetActiveProviders(NotificationChannel channel, string holderId, string countryCode, out Settings settings)
        {
            IList<INotificationProvider> messengers = null;
            Settings crds = null;
            messengers = Manager.GetManager().GetActiveProviders(channel, holderId, countryCode, out crds);
            settings = crds;
            return messengers;
        }

        public IList<INotificationProvider> GetActiveProviders(NotificationChannel channel, string holderId, string countryCode, NotificationClass notificationClass, out Settings settings)
        {
            IList<INotificationProvider> messengers = null;
            Settings crds = null;
            messengers = Manager.GetManager().GetActiveProviders(channel, holderId, countryCode, notificationClass, out crds);
            settings = crds;
            return messengers;
        }

        public INotificationProvider GetProvider(string providerId)
        {
            return Manager.GetManager().GetProvider(providerId);
        }

        public Boolean SaveSettings(Settings settings)
        {
            return Manager.GetManager().SaveSettings(settings);
        }

        public Boolean ClearSettings(string holderId, NotificationChannel channel)
        {
            return Manager.GetManager().ClearSettings(holderId, channel);
        }

        public Boolean SaveMessageReceiptSettings(Settings settings)
        {
            return Manager.GetManager().SaveMessageReceiptSettings(settings);
        }

        public Boolean SendSMS(Notification notification, string countryCode)
        {
            Boolean sendStatus = false;
            string providerId = "";
            TextMessageProvider provider = null;

            if (!string.IsNullOrWhiteSpace(notification.UseProvider))
            {
                providerId = notification.UseProvider;
                provider = GetProvider(providerId) as TextMessageProvider;
            }

            if (provider == null)
            {
                providerId = Manager.GetManager().GetActiveProviderId(NotificationChannel.SMS, notification.Class, countryCode, notification.CenterId, notification.OrganizationId);
                provider = GetProvider(providerId) as TextMessageProvider;
            }

            if (provider != null)
            {
                List<Notification> notifications = new List<Notification>();
                notifications.Add(notification);
                sendStatus = provider.Send(notifications);
            }
            return sendStatus;
        }

        public Boolean SendEMail(Notification notification, string countryCode)
        {
            Boolean sendStatus = false;
            string providerId = "";
            EMailProvider provider = null;

            if (!string.IsNullOrWhiteSpace(notification.UseProvider))
            {
                providerId = notification.UseProvider;
                provider = GetProvider(providerId) as EMailProvider;
            }

            if (provider == null)
            {
                providerId = Manager.GetManager().GetActiveProviderId(NotificationChannel.EMAIL, notification.Class, countryCode, notification.CenterId, notification.OrganizationId);
                provider = GetProvider(providerId) as EMailProvider;
            }

            if (provider != null)
            {
                List<Notification> notifications = new List<Notification>();
                notifications.Add(notification);
                sendStatus = provider.Send(notifications);
            }
            return sendStatus;
        }

    }
}
