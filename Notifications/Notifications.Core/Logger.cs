﻿using Core.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notifications.Core
{
    internal class NotificationsLogger
    {
        private static ILogWriter _logWriter;

        static NotificationsLogger()
        {
            _logWriter = LogManager.GetLogger("NotificationsProcessor");
        }

        public static ILogWriter Logger
        {
            get
            {
                return _logWriter;
            }
        }
    }
}
