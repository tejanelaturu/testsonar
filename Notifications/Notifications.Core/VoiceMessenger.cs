﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notifications.Core
{
    public class VoiceMessenger : Messenger
    {
        public VoiceMessenger(MessengerElement config)
            : base(config)
        {
            Type = MessengerType.VOICE;
        }

        protected override Response SendMessage(Message message)
        {
            Response response = new Response();

            try
            {
                if (!string.IsNullOrWhiteSpace(message.To))
                    response = Process(message);
            }
            catch (InvalidRecipient ex)
            {
                response.Status = SentStatus.Exception;
                response.Data = ex.Message;
            }
            catch (UnsupportedCountry ex)
            {
                response.Status = SentStatus.Exception;
                response.Data = ex.Message;
            }
            catch (Exception ex)
            {
                MessageProcessor.Logger.Error("Error in placing call", ex);
                response.Status = SentStatus.Exception;
                response.Data = ex.Message;
            }

            return response;
        }

        protected virtual Response Process(Message message)
        {
            throw new NotImplementedException();
        }

        public override void UpdateMessageStatus(string statusMessage)
        {
            Response response = GetStatus(statusMessage);
            if ((response != null) && (!string.IsNullOrWhiteSpace(response.ResponseId)))
                UpdateStatus(null, response);
        }

        public override void UpdateMessageStatusBulk()
        {
            IDBOps dbOps = null;
            SpaReaderEx objReader = null;
            try
            {
                DBFactory dbfactory = DBFactory.getInstance();
                dbOps = dbfactory.getDBOpsClass();
                dbOps.OpenConnection();
                dbOps.StoredProcedure("sp_Messaging_GetMessageResponses");
                dbOps.AddCharParameter("@MessengerId", ID);
                objReader = dbOps.Execute();

                if (objReader.HasRows)
                {
                    while (objReader.Read())
                    {
                        bool success = false;
                        string strResponseText = objReader.Get("ResponseText").ToString();
                        Int64 messageStatusPk = Convert.ToInt64(objReader.Get("PK"));
                        try
                        {
                            UpdateMessageStatus(strResponseText);
                            success = true;
                        }
                        catch (Exception ex)
                        {
                            MessageProcessor.Logger.Error("Error while updating message status", ex);
                        }
                        UpdateMessageResponseStatus(messageStatusPk, success);
                    }
                }

            }
            catch (Exception ex)
            {
                MessageProcessor.Logger.Error("Error while updating message status", ex);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (dbOps != null)
                    dbOps.CloseConnection();
            }
        }

        public void UpdateMessageResponseStatus(Int64 messageStatusPk, bool success)
        {
            IDBOps dbOps = null;
            try
            {
                DBFactory dbfactory = DBFactory.getInstance();
                dbOps = dbfactory.getDBOpsClass();
                dbOps.OpenConnection();
                dbOps.StoredProcedure("sp_Messaging_UpdateMessageResponseStatus");
                dbOps.AddLongParameter("@MessageStatusPk", messageStatusPk);
                dbOps.AddBoolParameter("@success", success);
                dbOps.Execute();
            }
            catch (Exception ex)
            {
                MessageProcessor.Logger.Error("Error while updating message status", ex);
            }
            finally
            {
                if (dbOps != null)
                    dbOps.CloseConnection();
            }
        }
        protected virtual Response GetStatus(string statusMessage)
        {
            throw new NotImplementedException();
        }

        protected override void UpdateStatus(string status, Response response)
        {
            IDBOps dbOps = null;
            //response.AdditionalData = null;
            try
            {
                DBFactory dbfactory = DBFactory.getInstance();
                dbOps = dbfactory.getDBOpsClass();
                dbOps.OpenConnection();
                dbOps.StoredProcedure("sp_Messaging_UpdateVoiceResponse");
                dbOps.AddCharParameter("@ResponseId", response.ResponseId);
                dbOps.AddIntParameter("@SentStatus", (int)response.Status);
                dbOps.AddCharParameter("@AdditionalData", response.AdditionalData);
                dbOps.AddIntParameter("@Duration", response.Clicks);
                dbOps.Execute();
            }
            catch (Exception ex)
            {
                MessageProcessor.Logger.Error("" + response.ResponseId, ex);
            }
            finally
            {
                dbOps.CloseConnection();
            }
        }

        protected override void PostSend(Message message, Response response)
        {
            if (response.Status == SentStatus.ReProcess)
            {
                MarkMessageAsReProcess(message, response);
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(message.Id))
                {
                    SaveVoiceResponse(message, response);
                }
            }
        }

        protected virtual void SaveVoiceResponse(Content.Message message, Response response) { }

        protected Boolean IsNumberValid(Content.VoiceMessage msg)
        {
            Boolean vaild = true;

            if (msg.CountryCode == "IN")
            {
                if (msg.To.Length < 10)
                    vaild = false;
            }
            else
            {
                if (string.IsNullOrWhiteSpace(msg.OrgPhoneRange))
                {
                    if (msg.To.Length < 7)
                        vaild = false;
                }
                else
                {
                    string[] range = msg.OrgPhoneRange.Split('-');
                    if (range.Length > 1)
                    {
                        int min, max;
                        int.TryParse(range[0], out min);
                        int.TryParse(range[1], out max);
                        if (!(msg.To.Length >= min && msg.To.Length <= max))
                            vaild = false;
                    }
                    else if (range.Length == 1)
                    {
                        int _range;
                        int.TryParse(range[0], out _range);
                        if (_range != msg.To.Length)
                            vaild = false;
                    }
                    else if (msg.To.Length < 7)
                        vaild = false;
                }
            }
            return vaild;
        }
    }
}
