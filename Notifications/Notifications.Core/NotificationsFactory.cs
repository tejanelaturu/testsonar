﻿
using Notifications.Core.Configuration;
using Notifications.Core.Enums;
using Notifications.Core.NotificationProviders;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notifications.Core
{
    public class NotificationsFactory
    {
        private MessagingSection _config = null;
        private IList<MessengerElement> _activeProviders = null;
        private IDictionary<string, INotificationProvider> _ProviderInstances = null;


        public NotificationsFactory()
        {
            _activeProviders = new List<MessengerElement>();
            _ProviderInstances = new Dictionary<string, INotificationProvider>(StringComparer.OrdinalIgnoreCase);
            LoadActiveProvidersConfig();
        }

        public INotificationProvider GetProvider(string ProviderId)
        {
            INotificationProvider Provider = null;

            if (!string.IsNullOrWhiteSpace(ProviderId))
            {
                if (_ProviderInstances.ContainsKey(ProviderId))
                {
                    Provider = _ProviderInstances[ProviderId] as NotificationProvider;
                }
                else
                {
                    MessengerElement requestedProvider = _activeProviders.FirstOrDefault(msngr => String.Equals(msngr.Id, ProviderId, StringComparison.CurrentCultureIgnoreCase) && msngr.Active == true && msngr.SendsMessages);

                    if (requestedProvider != null)
                    {
                        try
                        {
                            Type typeInfo = Type.GetType(requestedProvider.Implementor, true, false);
                            Provider = Activator.CreateInstance(typeInfo, requestedProvider) as NotificationProvider;
                            _ProviderInstances.Add(ProviderId, Provider);
                        }
                        catch (Exception ex)
                        {
                            NotificationsLogger.Logger.Error("Error while creating instance ", ex);
                        }
                    }
                    else
                    {
                        NotificationsLogger.Logger.Debug("No processor avaialble with id" + ProviderId);
                    }
                }
            }
            return Provider;
        }

        /* Order of picking messenger
         * Country specific and specific class
         * Country specific and all
         * All and specific class
         * all and all
         */

        public string GetDefaultProviderId(string countryCode, NotificationChannel channel, NotificationClass notificationClass)
        {
            string messengerId = null;
            MessengerElement defaultProvider = null;

            if (!string.IsNullOrWhiteSpace(countryCode))
            {
                defaultProvider = _activeProviders.FirstOrDefault(msngr => msngr.Type == channel && msngr.DefaultFor.Contains(countryCode) && (msngr.Supports == (int)notificationClass) && msngr.SendsMessages);
                if (defaultProvider == null)
                {
                    defaultProvider = _activeProviders.FirstOrDefault(msngr => msngr.Type == channel && msngr.DefaultFor.Contains(countryCode) && (msngr.Supports == (int)NotificationClass.ALL) && msngr.SendsMessages);
                }
            }
            
            if (defaultProvider == null)
            {
                defaultProvider = _activeProviders.FirstOrDefault(msngr => msngr.Type == channel && msngr.DefaultFor.Contains("ALL") && (msngr.Supports == (int)notificationClass) && msngr.SendsMessages);
            }
            if (defaultProvider == null)
            {
                defaultProvider = _activeProviders.FirstOrDefault(msngr => msngr.Type == channel && msngr.DefaultFor.Contains("ALL") && (msngr.Supports == (int)NotificationClass.ALL) && msngr.SendsMessages);
            }

            if (defaultProvider != null)
            {
                messengerId = defaultProvider.Id;
            }

            return messengerId;
        }

        public IList<INotificationProvider> GetActiveProviders(string countryCode, NotificationChannel channel, NotificationClass notificationClass)
        {
            IList<INotificationProvider> providers = null;

            IList<MessengerElement> elems = _activeProviders.Where(m => (m.AvailableIn.Contains(countryCode) || m.AvailableIn.ToUpper() == "ALL") && m.SendsMessages && m.Type == channel && m.Supports == (int)notificationClass).ToList();

            if (elems.Count > 0)
            {
                providers = new List<INotificationProvider>();
                foreach (MessengerElement elem in elems)
                {
                    INotificationProvider msngr = GetProvider(elem.Id);
                    providers.Add(msngr);
                }
            }
            return providers;
        }

        public IList<INotificationProvider> GetActiveProviders(string countryCode, NotificationChannel channel)
        {
            IList<INotificationProvider> providers = null;

            IList<MessengerElement> elems = _activeProviders.Where(m => (m.AvailableIn.Contains(countryCode) || m.AvailableIn.ToUpper() == "ALL") && m.SendsMessages && m.Type == channel).ToList();

            if (elems.Count > 0)
            {
                providers = new List<INotificationProvider>();
                foreach (MessengerElement elem in elems)
                {
                    INotificationProvider msngr = GetProvider(elem.Id);
                    providers.Add(msngr);
                }
            }
            return providers;
        }

        private Boolean LoadActiveProvidersConfig()
        {
            Boolean done = false;
            if (_config == null)
            {
                _config = ConfigurationManager.GetSection("notificationsSection") as MessagingSection;

                _activeProviders.Clear();

                foreach (MessengerElement element in _config.MessengerElements)
                {
                    if (element.Active)
                    {
                        _activeProviders.Add(element);
                    }
                }
            }

            return done;
        }
    }
}
