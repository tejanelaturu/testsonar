﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notifications.Core
{
    public class TupleComparer : IEqualityComparer<Tuple<string, string>>
    {
        public bool Equals(Tuple<string, string> lhs, Tuple<string, string> rhs)
        {
            return
              StringComparer.CurrentCultureIgnoreCase.Equals(lhs.Item1, rhs.Item1)
           && StringComparer.CurrentCultureIgnoreCase.Equals(lhs.Item2, rhs.Item2);
        }

        public int GetHashCode(Tuple<string, string> tuple)
        {
            return StringComparer.CurrentCultureIgnoreCase.GetHashCode(tuple.Item1)
                 ^ StringComparer.CurrentCultureIgnoreCase.GetHashCode(tuple.Item2);
        }
    }
}
