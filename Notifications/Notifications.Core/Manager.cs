﻿using Notifications.Core.Enums;
using Notifications.Core.NotificationProviders;
using SpaManagement.Organizations;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Util.DBUtil;
using Core;
using WebCache.Core;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Notifications.Core
{
    public class Manager
    {
        private NotificationsFactory _notificationsFactory = null;

        Manager _manager = null;

        private static Object objLock = new Object();

        internal static Manager GetManager()
        {
            //if (_manager == null)
            //{
            //    lock (objLock)
            //    {
            //if (_manager == null)
            //    _manager = new Manager();
            //    }
            //}
            return new Manager();
        }

        public Manager()
        {
            _notificationsFactory = new NotificationsFactory();
        }

        public IDictionary<Tuple<string, string>, Settings> GetChannelCredentials(NotificationChannel channel)
        {
            switch (channel)
            {
                case NotificationChannel.SMS: return SMSCredentials;
                case NotificationChannel.EMAIL: return EmailCredentials;
                case NotificationChannel.PHONE: return PhoneCredentials;
                case NotificationChannel.VOICE: return VoiceCredentials;
                default: return null;
            }
        }
        private ConcurrentDictionary<Tuple<string, string>, Settings> _smsCredentials = null;
        private ConcurrentDictionary<Tuple<string, string>, Settings> _emailCredentials = null;
        private ConcurrentDictionary<Tuple<string, string>, Settings> _phoneCredentials = null;
        private ConcurrentDictionary<Tuple<string, string>, Settings> _voiceCredentials = null;
        private ConcurrentDictionary<Tuple<string, string>, Settings> _pushCredentials = null;

        private Boolean _credentialsLoaded = false;

        
        internal IDictionary<Tuple<string, string>, Settings> SMSCredentials
        {
            get
            {
                if (!_credentialsLoaded)
                {

                    LoadCredentials();
                }
                return _smsCredentials;
            }
        }

        internal IDictionary<Tuple<string, string>, Settings> EmailCredentials
        {
            get
            {
                if (!_credentialsLoaded)
                {
                    LoadCredentials();
                }
                return _emailCredentials;
            }
        }

        internal IDictionary<Tuple<string, string>, Settings> PhoneCredentials
        {
            get
            {
                if (!_credentialsLoaded)
                {
                    LoadCredentials();
                }
                return _phoneCredentials;
            }
        }

        internal IDictionary<Tuple<string, string>, Settings> VoiceCredentials
        {
            get
            {
                if (!_credentialsLoaded)
                {
                    LoadCredentials();
                }
                return _voiceCredentials;
            }
        }
        internal IDictionary<Tuple<string, string>, Settings> PushCredentials
        {
            get
            {
                if (!_credentialsLoaded)
                {
                    LoadCredentials();
                }
                return _pushCredentials;
            }
        }

        private void LoadCredentials()
        {
            IDBOps dbOps = null;
            _smsCredentials = new ConcurrentDictionary<Tuple<string, string>, Settings>(new TupleComparer());
            _emailCredentials = new ConcurrentDictionary<Tuple<string, string>, Settings>(new TupleComparer());
            _phoneCredentials = new ConcurrentDictionary<Tuple<string, string>, Settings>(new TupleComparer());
            _voiceCredentials = new ConcurrentDictionary<Tuple<string, string>, Settings>(new TupleComparer());
            // PUSH Settings: New proc or changes in old proc?
            _pushCredentials = new ConcurrentDictionary<Tuple<string, string>, Settings>(new TupleComparer());
            try
            {
                dbOps = DBFactory.getInstance().getDBOpsClass();
                SpaReaderEx spaReader;
                if (dbOps.OpenConnection() == 0)
                {
                    dbOps.StoredProcedure("sp_pushprovider_getpushsettings");
                    dbOps.AddCharParameter("@HolderId", null);
                    spaReader = dbOps.Execute();

                    while (spaReader.Read())
                    {
                        int platformOS = 0;
                        string owner = null;
                        string appid = null;
                        string GCMKey = null, teamID = null, keyID = null, bundleID = null, appSecretAuth = null, random = null;
                        try
                        {
                            int.TryParse(spaReader.Get("PlatformOS").ToString(), out platformOS);
                            owner = spaReader.Get("OrganizationId").ToString();
                            appid = spaReader.Get("AppId").ToString();
                            Settings settings = new Settings(NotificationChannel.PUSH, owner);
                            settings.Configurations.Add("PlatformOS", "ANDROID");

                            GCMKey = spaReader.Get("GCMKey").ToString();
                            settings.Configurations.Add("Android_GCMKey", GCMKey);

                            Tuple<string, string> key = new Tuple<string, string>("ANDROID", owner);
                            _pushCredentials.TryAdd(key, settings);

                            settings = new Settings(NotificationChannel.PUSH, owner);
                            settings.Configurations.Add("PlatformOS", "APPLE");

                            teamID = spaReader.Get("TeamId").ToString();
                            keyID = spaReader.Get("KeyId").ToString();
                            bundleID = spaReader.Get("StoreBundleId").ToString();
                            var bin2str = spaReader.Get("AppSecretAuth");
                            if (bin2str != DBNull.Value)
                            {
                                byte[] barr = (byte[])bin2str;
                                //MemoryStream memobj = new MemoryStream(barr);
                                //StreamReader sr = new StreamReader(memobj);
                                //random = Encoding.UTF8.GetString(barr);
                                //appSecretAuth = Convert.ToBase64String(Encoding.UTF8.GetBytes(sr.ReadToEnd()));
                                appSecretAuth = Encoding.UTF8.GetString(barr);
                            }

                            settings.Configurations.Add("Apple_TeamId", teamID);
                            settings.Configurations.Add("Apple_BundleId", bundleID);
                            settings.Configurations.Add("Apple_KeyId", keyID);
                            settings.Configurations.Add("Apple_AppSecretAuth", appSecretAuth);

                            key = new Tuple<string, string>("APPLE", owner);
                            _pushCredentials.TryAdd(key, settings);


                        }
                        catch (Exception ex)
                        {
                            NotificationsLogger.Logger.Error("Error while loading settings for subscriber - " + owner + " ", ex);
                        }
                    }
                }
                else
                    throw new Exception("Could not open connection to database");
            }
            catch (Exception ex)
            {
                NotificationsLogger.Logger.Error("Error while loading settings", ex);
            }
            finally
            {
                if (dbOps != null)
                    dbOps.CloseConnection();
            }

            try
            {
                dbOps = DBFactory.getInstance().getDBOpsClass();
                SpaReaderEx spaReader;
                if (dbOps.OpenConnection() == 0)
                {
                    dbOps.StoredProcedure("Sp_messenger_getsettings");
                    dbOps.AddCharParameter("@HolderId", null);
                    spaReader = dbOps.Execute();

                    while (spaReader.Read())
                    {
                        int msngrType = 0;
                        int receiptType = 0;
                        bool enableReceiving = false;
                        bool enableZenotiConnect = false;
                        string owner = null;
                        string configuration = null;
                        int msngrClass = 0;
                        try
                        {
                            int.TryParse(spaReader.Get("MessengerType").ToString(), out msngrType);
                            owner = spaReader.Get("ConfigurationOwner").ToString();
                            configuration = spaReader.Get("Configuration").ToString();

                            int.TryParse(spaReader.Get("ReceiptType").ToString(), out receiptType);
                            enableReceiving = spaReader.Get("EnableReceiving").ToString() == "1";
                            enableZenotiConnect = spaReader.Get("EnableZenotiConnect").ToString() == "1";

                            Settings settings = new Settings((NotificationChannel)msngrType, owner);
                            if (receiptType != 0)
                            {
                                settings.ReceiptType = (ReceiptType)receiptType;
                            }
                            settings.EnableReceiving = enableReceiving;
                            settings.EnableZenotiConnect = enableZenotiConnect;
                            string[] strSettings = spaReader.Get("ReceiptSettings").ToString().Split(new string[] { "|;|" }, StringSplitOptions.None);

                            int.TryParse(spaReader.Get("MessengerClass").ToString(), out msngrClass);
                            foreach (string s in strSettings)
                            {
                                if (!string.IsNullOrWhiteSpace(s))
                                {
                                    string[] keyValuePair = s.Split(new string[] { "|:|" }, StringSplitOptions.None);
                                    if (keyValuePair.Length > 0 && !settings.ReceiptSettings.ContainsKey(keyValuePair[0]))
                                    {
                                        settings.ReceiptSettings.Add(keyValuePair[0], keyValuePair[1]);
                                    }
                                }
                            }

                            settings.LoadConfig(configuration);
                            Tuple<string, string> key = new Tuple<string, string>(((NotificationClass)msngrClass).ToString(), owner);
                            switch ((NotificationChannel)msngrType)
                            {
                                case NotificationChannel.EMAIL:
                                    _emailCredentials.TryAdd(key, settings);
                                    break;
                                case NotificationChannel.PHONE:
                                    _phoneCredentials.TryAdd(key, settings);
                                    break;
                                case NotificationChannel.VOICE:
                                    _voiceCredentials.TryAdd(key, settings);
                                    break;
                                case NotificationChannel.SMS:
                                    _smsCredentials.TryAdd(key, settings);
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {
                            NotificationsLogger.Logger.Error("Error while loading settings for subscriber - " + owner + " ", ex);
                        }
                    }
                }
                else
                {
                    throw new Exception("Could not open connection to database");
                }
            }
            catch (Exception ex)
            {
                NotificationsLogger.Logger.Error("Error while loading settings", ex);
            }
            finally
            {
                if (dbOps != null)
                    dbOps.CloseConnection();
            }
            _credentialsLoaded = true;
        }

        public Boolean SaveSettings(Settings settings)
        {
            Boolean success = false;
            IDBOps dbOps = null;
            
            try
            {
                dbOps = DBFactory.getInstance().getDBOpsClass();
                if (dbOps.OpenConnection() == 0)
                {
                    if(settings.Channel == NotificationChannel.PUSH)
                    {
                        dbOps.StoredProcedure("sp_pushprovider_savepushsettings");
                        dbOps.AddCharParameter("@HolderId", settings.HolderId);// HolderID
                        dbOps.AddIntParameter("@PlatformOS", (settings.ProviderId == "ANDROIDPUSH") ? 1 : 2);// PlatformOS
                        dbOps.AddCharParameter("@GCMKey", settings.Configurations.ContainsKey("GCMKey") ? settings.Configurations["GCMKey"] : null);// GCM Key If Exists
                        dbOps.AddCharParameter("@TeamId", settings.Configurations.ContainsKey("TeamId") ? settings.Configurations["TeamId"] : null);// TeamID if Exists
                        dbOps.AddCharParameter("@KeyId", settings.Configurations.ContainsKey("KeyId") ? settings.Configurations["KeyId"] : null);// KeyId if Exists
                        dbOps.AddCharParameter("@BundleId", settings.Configurations.ContainsKey("BundleId") ? settings.Configurations["BundleId"] : null);// BundleId if exists
                        byte[] barr = settings.Configurations.ContainsKey("AppSecretAuth") ? Encoding.UTF8.GetBytes(settings.Configurations["AppSecretAuth"]) : null;
                        dbOps.AddVarBinaryParameter("@AppSecretAuth", barr);// AppSecretAuth if exists
                    }
                    else
                    {
                        dbOps.StoredProcedure("Sp_messenger_savesettings");
                        dbOps.AddCharParameter("@HolderId", settings.HolderId);
                        dbOps.AddIntParameter("@MessengerType", (int)settings.Channel);
                        dbOps.AddCharParameter("@Configuration", settings.GetConfig());
                        dbOps.AddCharParameter("@ModifiedBy", null);
                        dbOps.AddCharParameter("@MessengerId", settings.ProviderId);
                        dbOps.AddBoolParameter("@EnableInternationalSms", settings.EnableInternationalSMS);
                        if (settings.Channel == NotificationChannel.SMS)
                        {
                            NotificationProvider messenger = _notificationsFactory.GetProvider(settings.ProviderId) as NotificationProvider;
                            dbOps.AddIntParameter("@MaxLength", messenger.MaxLength);
                            dbOps.AddIntParameter("@MaxLengthUnicode", messenger.MaxLengthUnicode);
                        }
                    }

                    dbOps.Execute();
                    success = true;
                }
                else
                {
                    throw new Exception("Could not open connection to database");
                }
            }
            catch (Exception ex)
            {
                NotificationsLogger.Logger.Error("Error while saving settings", ex);
            }
            finally
            {
                if (dbOps != null)
                    dbOps.CloseConnection();
            }
            return success;
        }

        public Boolean SaveMessageReceiptSettings(Settings settings)
        {
            Boolean success = false;
            IDBOps dbOps = null;
            try
            {
                dbOps = DBFactory.getInstance().getDBOpsClass();
                if (dbOps.OpenConnection() == 0)
                {
                    dbOps.StoredProcedure("spSaveCenterMessageRecieptSettings");
                    dbOps.AddCharParameter("@CenterId", settings.HolderId);
                    dbOps.AddIntParameter("@ReceiptType", (int)settings.ReceiptType);
                    dbOps.AddIntParameter("@ReceiptMediumType", (int)settings.Channel);
                    dbOps.AddCharParameter("@AdditionalData", string.Join("|;|", settings.ReceiptSettings.Select(x => x.Key + "|:|" + x.Value).ToArray()));
                    dbOps.AddCharParameter("@ProviderId", settings.ProviderId);
                    dbOps.AddBoolParameter("@EnableReceiving", settings.EnableReceiving);
                    dbOps.AddCharParameter("@ModifiedBy", null);
                    dbOps.AddBoolParameter("@EnableZenotiConnect", settings.EnableZenotiConnect);

                    dbOps.Execute();
                    success = true;
                }
                else
                {
                    throw new Exception("Could not open connection to database");
                }
            }
            catch (Exception ex)
            {
                NotificationsLogger.Logger.Error("Error while saving settings", ex);
            }
            finally
            {
                if (dbOps != null)
                    dbOps.CloseConnection();
            }
            return success;
        }

        public Boolean ClearSettings(string HolderId, NotificationChannel channel)
        {
            Boolean success = false;
            IDBOps dbOps = null;

            try
            {
                dbOps = DBFactory.getInstance().getDBOpsClass();
                if (dbOps.OpenConnection() == 0)
                {
                    dbOps.StoredProcedure("Sp_messenger_deletesettings");
                    dbOps.AddCharParameter("@HolderId", HolderId);
                    dbOps.AddIntParameter("@MessengerType", (int)channel);

                    dbOps.Execute();
                    success = true;
                }
                else
                {
                    throw new Exception("Could not open connection to database");
                }
            }
            catch (Exception ex)
            {
                NotificationsLogger.Logger.Error("Error while clearing settings", ex);
            }
            finally
            {
                if (dbOps != null)
                    dbOps.CloseConnection();
            }
            return success;
        }

        public IList<INotificationProvider> GetActiveProviders(NotificationChannel channel, string holderId, string countryCode, out Settings settings)
        {
            IList<INotificationProvider> messengers = null;
            Settings crds = null;
            messengers = _notificationsFactory.GetActiveProviders(countryCode, channel);
            Tuple<string, string> key = new Tuple<string, string>(NotificationClass.ALL.ToString(), holderId);

            if (channel == NotificationChannel.SMS)
            {
                if (SMSCredentials.ContainsKey(key))
                    crds = SMSCredentials[key];
            }
            else if (channel == NotificationChannel.PHONE)
            {
                if (PhoneCredentials.ContainsKey(key))
                    crds = PhoneCredentials[key];
            }
            else if (channel == NotificationChannel.VOICE)
            {
                if (VoiceCredentials.ContainsKey(key))
                    crds = VoiceCredentials[key];
            }
            else
            {
                if (EmailCredentials.ContainsKey(key))
                    crds = EmailCredentials[key];
            }

            settings = crds;
            return messengers;
        }

        public IList<INotificationProvider> GetActiveProviders(NotificationChannel channel, string holderId, string countryCode, NotificationClass notificationClass, out Settings settings)
        {
            IList<INotificationProvider> messengers = null;
            Settings crds = null;
            messengers = _notificationsFactory.GetActiveProviders(countryCode, channel, notificationClass);
            Tuple<string, string> key = new Tuple<string, string>(NotificationClass.ALL.ToString(), holderId);
            Tuple<string, string> keyAndroid = new Tuple<string, string>("ANDROID", holderId); // For Push
            Tuple<string, string> keyApple = new Tuple<string, string>("APPLE", holderId); // For Push

            if (channel == NotificationChannel.SMS)
            {
                if (SMSCredentials.ContainsKey(key))
                    crds = SMSCredentials[key];
            }
            else if (channel == NotificationChannel.PHONE)
            {
                if (PhoneCredentials.ContainsKey(key))
                    crds = PhoneCredentials[key];
            }
            else if (channel == NotificationChannel.VOICE)
            {
                if (VoiceCredentials.ContainsKey(key))
                    crds = VoiceCredentials[key];
            }
            else if (channel == NotificationChannel.PUSH)
            {
                Settings s1 = null, s2 = null;
                if (PushCredentials.ContainsKey(keyAndroid))
                    s1 = PushCredentials[keyAndroid];

                if (PushCredentials.ContainsKey(keyApple))
                    s2 = PushCredentials[keyApple];

                crds = new Settings(NotificationChannel.PUSH, holderId);

                if (s1 != null)
                {
                    crds.Configurations.Add("Android_GCMKey", s1.Configurations["Android_GCMKey"]);
                }

                if (s2 != null)
                {
                    crds.Configurations.Add("Apple_TeamId", s2.Configurations["Apple_TeamId"]);
                    crds.Configurations.Add("Apple_KeyId", s2.Configurations["Apple_KeyId"]);
                    crds.Configurations.Add("Apple_BundleId", s2.Configurations["Apple_BundleId"]);
                    crds.Configurations.Add("Apple_AppSecretAuth", s2.Configurations["Apple_AppSecretAuth"]);
                }
            }
            else
            {
                if (EmailCredentials.ContainsKey(key))
                    crds = EmailCredentials[key];
            }

            settings = crds;
            return messengers;
        }

        public IList<INotificationProvider> GetAllActiveProviders(NotificationChannel channel, string organizationId, string centerId)
        {
            IList<INotificationProvider> providers = null;
            Settings crds = null;

            if (string.IsNullOrWhiteSpace(centerId))
            {
                DataTable mData = new DataTable();
                SpaReaderEx rdr = SpaOrganization.GetOrganizationCenters(organizationId, true, -1, 0, null);
                if (rdr.Read())
                {
                    rdr.NextResult();
                    if (rdr.HasRows)
                    {
                        mData = rdr.GetDataTable();
                        providers = (from row in mData.AsEnumerable()
                                     where SMSCredentials.ContainsKey(new Tuple<string, string>(NotificationClass.ALL.ToString(), row["CenterId"].ToString()))
                                     group row by new { ProviderId = SMSCredentials[new Tuple<string, string>(NotificationClass.ALL.ToString(), row["CenterId"].ToString())].ProviderId } into g
                                     select _notificationsFactory.GetProvider(g.Key.ProviderId) as INotificationProvider).ToList();
                    }
                }
                if (SMSCredentials.ContainsKey(new Tuple<string, string>(NotificationClass.ALL.ToString(), organizationId)))
                    crds = SMSCredentials[new Tuple<string, string>(NotificationClass.ALL.ToString(), organizationId)];
                if (crds != null)
                {
                    providers = providers == null ? new List<INotificationProvider>() : providers;
                    INotificationProvider provider = _notificationsFactory.GetProvider(crds.ProviderId) as INotificationProvider;
                    if (providers != null)
                        providers.Add(provider);
                }
            }
            else
            {
                Tuple<string, string> key = new Tuple<string, string>(NotificationClass.ALL.ToString(), centerId);
                if (channel == NotificationChannel.SMS)
                {
                    if (SMSCredentials.ContainsKey(key))
                        crds = SMSCredentials[key];
                }
                else if (channel == NotificationChannel.PHONE)
                {
                    if (PhoneCredentials.ContainsKey(key))
                        crds = PhoneCredentials[key];
                }
                else if (channel == NotificationChannel.VOICE)
                {
                    if (VoiceCredentials.ContainsKey(key))
                        crds = VoiceCredentials[key];
                }
                else
                {
                    if (EmailCredentials.ContainsKey(key))
                        crds = EmailCredentials[key];
                }
                if (crds != null)
                {
                    providers = new List<INotificationProvider>();
                    providers.Add(_notificationsFactory.GetProvider(crds.ProviderId) as INotificationProvider);
                }
            }
            return providers;
        }

        public string GetActiveProviderId(NotificationChannel channel, NotificationClass notificationClass, string countryCode, string centerId, string organizationId)
        {
            string messengerId = null;
            Tuple<string, string> specificKey;
            Tuple<string, string> allKey;
            IDictionary<Tuple<string, string>, Settings> creds = null;
            switch ((NotificationChannel)channel)
            {
                case NotificationChannel.EMAIL:
                    creds = EmailCredentials;
                    break;
                case NotificationChannel.PHONE:
                    creds = PhoneCredentials;
                    break;
                case NotificationChannel.VOICE:
                    creds = VoiceCredentials;
                    break;
                case NotificationChannel.SMS:
                    creds = SMSCredentials;
                    break;
            }
            if (creds != null)
            {
                //retrieve from center
                specificKey = new Tuple<string, string>(notificationClass.ToString(), centerId);
                allKey = new Tuple<string, string>(NotificationClass.ALL.ToString(), centerId);
                Settings settings = null;
                if (creds.ContainsKey(specificKey))
                {
                    creds.TryGetValue(specificKey, out settings);
                }
                else if (creds.ContainsKey(allKey))
                {
                    creds.TryGetValue(allKey, out settings);
                }
                //fall back to org
                if (settings == null)
                {
                    specificKey = new Tuple<string, string>(notificationClass.ToString(), organizationId);
                    allKey = new Tuple<string, string>(NotificationClass.ALL.ToString(), organizationId);
                    if (creds.ContainsKey(specificKey))
                    {
                        creds.TryGetValue(specificKey, out settings);
                    }
                    else if (creds.ContainsKey(allKey))
                    {
                        creds.TryGetValue(allKey, out settings);
                    }
                }
                if (settings != null)
                {
                    messengerId = settings.ProviderId;
                }
                else
                {
                    //As there is no messenger configured pick any messenger that are set as default in configuration
                    messengerId = _notificationsFactory.GetDefaultProviderId(countryCode, channel, notificationClass);
                }
            }

            return messengerId;
        }

        public bool HasActiveProviders(NotificationChannel channel, string centerId, string organizationId, string countryCode, NotificationClass notificationClass = NotificationClass.ALL)
        {
            string providerId = "";
            providerId = GetActiveProviderId(channel, notificationClass, countryCode, centerId, organizationId);

            if (string.IsNullOrWhiteSpace(providerId))
                return false;
            else
                return true;
        }

        public INotificationProvider GetProvider(string providerId)
        {
            return _notificationsFactory.GetProvider(providerId);
        }


        public string getPushSettings(long appFK,string strOrganizationId )
        {

            string apiSettingsKey = "pushmesssagekey";
            SpaReaderEx spareader = null;
            string reader = WebCacheManager.ActiveInstance.GetCacheItem(strOrganizationId, null, apiSettingsKey, "" + appFK);
            if (string.IsNullOrWhiteSpace(reader))
            {
                IDBOps dbOps = null;
                try
                {
                    dbOps = DBFactory.getInstance().getDBOpsClass();
                    if (dbOps.OpenConnection() == 0)
                    {
                        dbOps.StoredProcedure("Sp_notifications_getpushsettings");
                        dbOps.AddLongParameter("@appFk", appFK);
                        spareader = dbOps.Execute();
                        DataTable dt = spareader.GetDataTable();
                        DataRow dr = (dt.Rows.Count > 0) ? dt.Rows[0] : null;
                        reader = JArray.FromObject(dt, JsonSerializer.CreateDefault(new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore })).FirstOrDefault().ToString();
                        // DataRow parsed correctly to JSON String. 

                        WebCacheManager.ActiveInstance.AddCacheItem(strOrganizationId, null, apiSettingsKey, "" + appFK, reader);
                    }

                }
                catch (Exception ex)
                {
                    NotificationsLogger.Logger.Error("Error while getting settings", ex);
                }
                finally
                {
                    if (dbOps != null)
                        dbOps.CloseConnection();
                }
                return reader;
                
            }
            else {
                return reader;
            }
            
        }
    }
}