﻿using Notifications.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notifications.Core
{
    public class SenderValidationResult
    {
        public ValidationLevel level { get; set; }
        public bool IsValid { get; set; }
        public string Comments { get; set; }

        public SenderValidationResult()
        {
            level = ValidationLevel.NotValidated;
            IsValid = false;
            Comments = "";
        }
    }
}
