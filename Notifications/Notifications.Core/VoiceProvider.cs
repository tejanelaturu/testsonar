﻿using Notifications.Core.Exceptions;
using Notifications.Core.Content;
using Notifications.Core.Enums;
using Notifications.Core.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Util.DBUtil;
using Core;
using Notifications.Core.NotificationProviders;
using System.Data;

namespace Notifications.Core
{
    public class VoiceProvider : NotificationProvider
    {
        public VoiceProvider(MessengerElement config)
            : base(config)
        {
            Channel = NotificationChannel.VOICE;
        }


        protected override IEnumerable<Response> Process(IEnumerable<Notification> notifications)
        {
            throw new NotImplementedException();
        }

        public override void PollMessageStatus()
        {
            // call and update the status using ReadStatusFromReponse and then UpdateStatusFromResponse
        }

        protected override Response ReadStatusFromReponse(string statusMessage)
        {
            throw new NotImplementedException();
        }

        protected override void UpdateStatusFromResponse(DataTable status, Response response)
        {
            // update status to db
        }

        protected override IDictionary<string, string> GetDataForMessageReply(int messageType, IDictionary<string, string> msgData, object content)
        {
            throw new NotImplementedException();
        }

        protected Boolean IsNumberValid(TextMessageNotification textMessageNotification)
        {
            Boolean vaild = true;

            if (textMessageNotification.CountryCode == "IN")
            {
                if (textMessageNotification.To.Length < 10)
                    vaild = false;
            }
            else
            {
                if (string.IsNullOrWhiteSpace(textMessageNotification.OrgPhoneRange))
                {
                    if (textMessageNotification.To.Length < 7)
                        vaild = false;
                }
                else
                {
                    string[] range = textMessageNotification.OrgPhoneRange.Split('-');
                    if (range.Length > 1)
                    {
                        int min, max;
                        int.TryParse(range[0], out min);
                        int.TryParse(range[1], out max);
                        if (!(textMessageNotification.To.Length >= min && textMessageNotification.To.Length <= max))
                            vaild = false;
                    }
                    else if (range.Length == 1)
                    {
                        int _range;
                        int.TryParse(range[0], out _range);
                        if (_range != textMessageNotification.To.Length)
                            vaild = false;
                    }
                    else if (textMessageNotification.To.Length < 7)
                        vaild = false;
                }
            }
            return vaild;
        }
    }
}
