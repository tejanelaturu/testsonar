﻿using Notifications.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notifications.Core
{
    public class Settings
    {
        private string _providerId;
        public NotificationChannel Channel { get; set; }
        public ReceiptType ReceiptType { get; set; }
        public Boolean EnableReceiving { get; set; }
        public Boolean EnableZenotiConnect { get; set; }
        public IDictionary<string, string> ReceiptSettings { get; set; }
        public string HolderId { get; set; }
        //public string ParentId { get; set; }
        public IDictionary<string, string> Credentials { get; set; }
        public IDictionary<string, string> Configurations { get; set; }
        public Boolean EnableInternationalSMS { get; set; }

        public string ProviderId
        {
            get
            {
                return _providerId.ToUpper();
            }
            set
            {
                _providerId = value;
            }
        }

        public Settings()
        {
            Credentials = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            Configurations = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            ReceiptSettings = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
        }

        public Settings(NotificationChannel channel, string holderId)
        {
            Channel = channel;
            HolderId = holderId;
            //ParentId = parentId;
            Credentials = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            Configurations = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            ReceiptSettings = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
        }

        public Boolean IsConfigured()
        {
            return (Credentials.Count > 0);
        }

        public Settings Clone()
        {
            Settings clone = new Settings(this.Channel, this.HolderId);
            clone.ProviderId = this.ProviderId;
            foreach (KeyValuePair<string, string> kvp in this.Credentials)
            {
                clone.Credentials.Add(kvp.Key, kvp.Value);
            }
            foreach (KeyValuePair<string, string> kvp in this.Configurations)
            {
                clone.Configurations.Add(kvp.Key, kvp.Value);
            }
            clone.EnableReceiving = this.EnableReceiving;
            clone.EnableZenotiConnect = this.EnableZenotiConnect;
            clone.ReceiptType = this.ReceiptType;
            clone.ReceiptSettings = this.ReceiptSettings;
            return clone;
        }

        public void LoadConfig(string config)
        {
            if (!string.IsNullOrEmpty(config))
            {
                string[] configParts = config.Split(new string[] { "|#|" }, StringSplitOptions.None);
                ProviderId = configParts[0];

                if (configParts.Length > 1)
                {
                    configParts = configParts[1].Split(new string[] { "|-|" }, StringSplitOptions.None);
                    if (configParts.Length >= 1)
                    {
                        foreach (string token in configParts[0].Split(new string[] { "|;|" }, StringSplitOptions.None))
                        {
                            if (!string.IsNullOrWhiteSpace(token))
                            {
                                string[] tokenParts = token.Split(new string[] { "|:|" }, StringSplitOptions.None);

                                string key = tokenParts[0];
                                string value = tokenParts[1];

                                if (!string.IsNullOrEmpty(key))
                                {
                                    Credentials.Add(key, value);
                                }
                            }
                        }
                    }
                    if (configParts.Length >= 2)
                    {
                        foreach (string token in configParts[1].Split(new string[] { "|;|" }, StringSplitOptions.None))
                        {
                            if (!string.IsNullOrWhiteSpace(token))
                            {
                                string[] tokenParts = token.Split(new string[] { "|:|" }, StringSplitOptions.None);

                                string key = tokenParts[0];
                                string value = tokenParts[1];

                                if (!string.IsNullOrEmpty(key))
                                {
                                    Configurations.Add(key, value);
                                }
                            }
                        }
                    }
                }
            }
        }

        public string GetConfig()
        {
            StringBuilder strBuilder = new StringBuilder("");

            strBuilder.Append(string.Format("{0}|#|", ProviderId));
            foreach (KeyValuePair<string, string> kvp in Credentials)
            {
                strBuilder.Append(string.Format("{0}|:|{1}|;|", kvp.Key, kvp.Value));
            }
            strBuilder.Append("|-|");
            foreach (KeyValuePair<string, string> kvp in Configurations)
            {
                strBuilder.Append(string.Format("{0}|:|{1}|;|", kvp.Key, kvp.Value));
            }

            return strBuilder.ToString();
        }
    }

}
